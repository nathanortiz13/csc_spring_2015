/* 
 * File:   main.cpp
 * Author: natha_000
 *
 * Created on March 5, 2015, 11:38 AM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    // receive two numbers from user

    int x, y;

    cout << "Enter a number less than 1000 for X: ";
    cin >> x;
    cout << "Enter a number less than 1000 for Y: ";
    cin >> y;

    if (x > 1000) {
        cout << "Number is too big.";
    } else if (y > 1000) {
        cout << "Number is too big.";
    } else {
        cout << "X=" << x << setw(x) << "Y=" << y << endl;
        cout << "-------------------------------------------------" << endl;
        cout << "X=" << y << setw(x) << "Y=" << x << endl;
    }

    return 0;
}

