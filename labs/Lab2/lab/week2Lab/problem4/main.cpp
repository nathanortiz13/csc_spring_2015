/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 4, 2015, 4:24 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
   
    int x, userStatement;
    
    cout << "If x is currently storing ";
    cin >> x;
    cout << "your statement will have it store ";
    
    userStatement = ( x + 5);
    
    cout << userStatement;
    cout << ".";
            
    return 0;
}

