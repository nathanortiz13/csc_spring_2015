/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 4, 2015, 4:44 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
   
    double test1, test2, test3, test_Average;
    
    cout << "Enter first test score: ";
    cin >> test1;
    cout << "Enter second test score: ";
    cin >> test2;
    cout << "Enter third tests score: ";
    cin >> test3;
    
    test_Average = (test1 + test2 + test3)/3;
            
    cout << "Your average is ";
    cout << test_Average;
    cout << "." << endl;
    cout << endl;
// Part b
    double score1, score2, score3, average;
    
    cout << "Enter your three test scores in the format (__ __ __):";
    cin >> score1 >> score2 >> score3;
    
    average = ( score1 + score2 + score3 )/3;
    
    cout << "Your Average is " << average << ".";
    
    return 0;
}

