/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 25, 2015, 4:44 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    int a = 5;
    int b = 10;
    int c = a;
    cout << "a:" << a << " " << "b:" << b << endl;
    
    a =b;
    b =a;
    a =c;
    cout << "a:" << a << " " << "b:" << b << endl;
    
    
    return 0;
}

