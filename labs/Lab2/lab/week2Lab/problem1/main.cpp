/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on February 25, 2015, 4:30 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

string helloMyNameIsHal;

helloMyNameIsHal = "Hello, my name is Hal!";

cout << helloMyNameIsHal;

string userInput;
cout << endl << "What is your name?";
cin >> userInput;
cout << "Hello," << userInput << ". I am glad to meet you." << endl;

    return 0;
}

