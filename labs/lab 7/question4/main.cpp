/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 1, 2015, 4:40 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
void swapnum( double num1, double num2)
{
    double temp = num2;
    num2 = num1;
    num1 = temp;
    
}

int main(int argc, char** argv) 
{
    
    double x= 5.8, y = 1.2;
    swapnum (x,y);
    cout << x << endl;
    cout << y << endl;
    
    

    return 0;
}

void swapReferenced (double & num1, double & num2)
{
    double temp = num2;
    num2 = num1;
    num1 = temp;
}

