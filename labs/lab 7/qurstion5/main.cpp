/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 1, 2015, 4:54 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */

bool palindrome (string userInput)
{
    for (int i = 0; i < userInput.size(); i++)
    {
        if (userInput[i] != userInput[userInput.size()-1-i])
        {
            return false;
        }
    }
    return true;
}

int main(int argc, char** argv) {
    
    cout << "Enter a word to see if its a Palindrome: ";
    string userInput;
    cin >> userInput;
    if (palindrome(userInput))
    {
        cout << "Word is a palindrome.";
    }
    else
    {
        cout << "Word is not a palindrome.";
    }

    return 0;
}

