/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 16, 2015, 4:23 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    int number;
    cout << "Please enter a number between 1-100 for a determine grade." << endl;
    cin >> number;
    
    if ( number >= 90 )
    {
        cout << "Grade: A";
    }
    
    else if ( number >= 80 && number <90 )
    {
        cout << "Grade: B";
    }
    
     else if ( number >= 70 && number <80 )
    {
        cout << "Grade: C";
    }
    
    else if ( number >= 60 && number <70 )
    {
        cout << "Grade: D";
    }
    
    else 
    {
        cout << "Grade: F";
    }

    return 0;
}

