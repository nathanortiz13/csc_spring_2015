/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 16, 2015, 4:48 PM
 */

#include <cstdlib>
#include <iostream>
#include <iomanip>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
   
    for ( int i = 0; i < 10; i++ )
    {
        int value;
        cout << "Enter a value: ";
        cin >> value;
        cout << value << endl;
    }
   
    return 0;
}

