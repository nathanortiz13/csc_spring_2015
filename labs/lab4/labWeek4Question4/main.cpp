/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 17, 2015, 12:05 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    string grade;
    cout << "Enter a Grade letter in all from B- to A+: ";
    cin >> grade;
    
    if ( grade == "A+")
    {
        cout << "100+";
    }
    else if (grade == "A")
    {
        cout << "93-100";
        
    }
    else if (grade == "A-" )
    {
        cout << "90-92.9";
    }
    else if (grade == "B+" )
    {
        cout << "87-89.9";
    }
    else if (grade == "B" )
    {
        cout << "83-86.9";
    }
    else if (grade == "B-" )
    {
        cout << "80-82.9";
    }
    else
    {
        cout << "Error. input not in the grade range B- to A+";
    }

    
    return 0;
}

