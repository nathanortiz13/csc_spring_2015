/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 22, 2015, 2:51 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) 
{
//    //using cin
//    string word1;
//    cout << "Enter a word: ";
//    cin >> word1;
//    
//    cout << "You've entered: " << word1 << endl;
//    
//    //use another stream >> operator
//    // >> stream operator reads from the buffer
//    // until a space or a newline
//    // ignores the space and newline
//    // DOES NOT REMOVE \n FROM BUFFER
//    string word;
//    cin >> word;
//    cout << "You've entered: " << word << endl;
    
    
    //use getline to read entire lines
    //getline reads everything until a newline
    //removes newline from buffer
    cout << "Enter a line: " << endl;
    string word;
    getline(cin, word);
    cout << "You've entered: " << word;
    
    //GET member function
    //Get reads a single character
    char c;
    cout << "Enter a character: " << endl;
    cin.get(c); // c=cin.get()
    cout << "You've entered: " << c << endl;
    
     return 0;
}

