/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 20, 2015, 3:30 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;
//step1: describe function
//step2: describe parameters
//step3: describe return type/value
/**
 * Fibbonacci function calculates the value of the Fibbonacci sequence
 * @param num the desired sequence
 * @return the value at a given sequence as an int
 */
int fibb(int num)
{
    cout << "Fibb: " <<num << endl;
    
    if (num == 2)
    return 1;
    else if (num == 1)
    return 0;
    else
        return fibb(num-1) + fibb(num-2);
}
/*
 * 
 */
int main(int argc, char** argv) 
{
    int num;
    cout << "Enter a number: ";
    cin >> num;
    
    cout << "Fibb of " << num << "is : " << fibb(num);
    return 0;
}

