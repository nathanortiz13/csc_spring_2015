/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 4, 2015, 3:37 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

void output (int[],int,int);
void addValue (int[], int, int&, int);
/*
 * 
 */
int main(int argc, char** argv){

    //create a partially filled array
    //need a size tracker, capacity and array
    int capacity =10;
    int size = 0;
    int numbers[capacity];
    
    addValue(numbers, capacity, size, 10);
    addValue(numbers, capacity, size, 20);
    output(numbers, capacity,size);
    return 0;
}

void output (int a[], int cap, int size)
{
    for (int i=0; i < cap; i++)
    {
        cout << a[i] << " ";
    }
}

void addValue (int a[], int cap, int& size, int value)
{
//check if size is value
    if (size < cap)
    {
        a[size] = value;
        size++;
    }
}