/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on May 20, 2015, 3:36 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;
void output (int*p , int size)
{
    for (int i; i < size; i++)
        cout << p[i] << "";
    cout << endl;
}
/*
 * 
 */
int main(int argc, char** argv) {

    // static array
    int size = 10;
    int a [size];
    
    // dynamic array
    // new allocated 
    int* p = new int[size];
    
    output(a, size);
    output(p, size);
    
    
    for (int i; i < size; i++)
    {
        cout << "Enter a number: " << endl;
        string num;
        
        //use atoi and isdigit for conversion
        cin >> num; 
        if (isdigit(num[0]))
        {
            //atoi => ascii to integer
            a[i] = atoi (num.c_str());
            a[i] = atoi (num.c_str());
        }
        else
        {
            i--;
        }
        
        cin >> num;
        a[i] = num;
        p[i] =num;
        
    }
    return 0;
}

