/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 4, 2015, 3:32 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {
    
    //Get user input
    cout << "Please enter a number: ";
    
    int num;
    cin >> num;
    
    if (num > 10)
    {
        cout << "Your number is bigger than 10";
    }
    else
    {
        cout << "your number is smaller than 10.";
    }
    
    // Get another number from the user
    //Tell if odd or even
    // Tell if positive or negative
    cout << endl << "Please enter another number: ";
    
    //Get another number from the user
    cin >> num;
    int rem = num %2;
    if (rem == 0)
    {
        cout << "Even number!";
    }
    else
    {
        cout << "Odd number!";
    }
    cout << endl;
    if (num <0)
    {
        cout << "Negative Number!";
    }
    else
    {
        cout << "Positive number!";
    }
    
    return 0;
}

