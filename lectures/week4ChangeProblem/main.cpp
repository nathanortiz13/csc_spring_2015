/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 11, 2015, 3:37 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    // Get user input
    cout << "Enter how much you owe: ";
    
    double owed;
    cin >> owed;
    
    cout << "Enter what you paid: ";
    
    double paid;
    cin >> paid;
    
    //calculate the change
    double change = paid - owed;
    
    // convert change to pennies
    // add an offset to avoid round off error
    int changeInt = (change + .0005) * 100;
    
    int dollars = changeInt / 100;
    // get remaining change
    changeInt %= 100;
    
    
    // changeInt = changeInt % 100
    
    //quarters
    int quarters = changeInt/ 25;
    changeInt %= 25;
    //dimes
    int dimes = changeInt/ 10;
   changeInt %= 10;
    //nickles
    int nickles = changeInt/ 5;
    changeInt %= 5;
    //pennies
    int pennies = changeInt/ 1;
    changeInt %= 1;
    
    //output amounts
    cout << "Dollars: " << dollars << endl;
    cout << "Quarters: " << quarters << endl;
    cout << "Dimes: " << dimes << endl;
    cout << "Nickles: " << nickles << endl;
    cout << "Pennies: " << pennies ;
    
    return 0;
}

