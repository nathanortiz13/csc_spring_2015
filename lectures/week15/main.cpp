/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on June 1, 2015, 3:22 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

class Vehicle
/*
 * 
 */
class Vehicle 
{
// fields of access
private: 
    // attributes
    int wheels;
public:
    // always need default constructor
    Vehicle();
    Vehicle (int);
    // Getters/setters
    int getWheels();
    void setWheels();
    //Output Function
    void output(ostream &);
};

//default constructor
Vechile::Vehicle()
:wheels (0) {}

vehicle::Vehicle(int wheels)
{
    this->wheels = wheels;
}

// ostream allows cout AND ofstream data

void Vehicle::output(ostream& out)
{
    out << "wheels: " << wheels << endl;
}


//inheritance
class spaceship : vehicle // is inherits from 
{
private:
    string os;
public:
    spaceship(); //default
    sapceship(string);
    spaceship(string, int );
    spaceship(ostream &);
    void output(ostream &);
    
};

spaceship::spaceship()
{
//create the base
    //this ->Vehicle::Vehicle;
}


void spaceship::output(ostream& out)
{
    cout << "Operating System: " << os << endl;
}

spaceship::spaceship(string os)
:os(os), vehicle(wheels)
{}

int main(int argc, char** argv) {
    
    //use default constructor 
    //v is known as the place holder of the
    // vehicle object
    cout << "Outputting a default vahicle: " << endl;
    Vehicle v;
    v.output(cout);
    
    //use other constructor
    Vehicle v2(4);
    v2.output(cout);
    
//returns a vehicle
//name of the function is v3
// no parameters
    vehicle v3();

    cout << "Outputting a default spaceship: " << endl;
    //create a sapceship
    spaceship s;
    s.output(cout);
    
    
    spaceship s2("windows", 6);
    s2.output()
    return 0;
}

