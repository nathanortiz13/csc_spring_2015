/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 11, 2015, 4:47 PM
 */

#include <cstdlib>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    float singles, doubles, triples, homeruns, atBats, slugging_percentage;
    
    cout << "Please enter the number of singles: " << endl;
    cin >> singles;
    cout << "Please enter the number of doubles: " << endl;
    cin >> doubles;
    cout << "Please enter the number of triples: " << endl;
    cin >> triples;
    cout << "Please enter the number of home runs: " << endl;
    cin >> homeruns;
    cout << "Please enter the number at bats: " << endl;
    cin >> atBats;
    if (atBats < singles + doubles + triples + homeruns)
    {
        cout << "Error. The number of at bats must be greater than or equal to the number of singles.";
    }
    else {
        slugging_percentage = (singles+2*doubles+3*triples+4*homeruns)/ atBats;
        cout << "Slugging percentage is: " << slugging_percentage << endl;
        
    return 0;
}

