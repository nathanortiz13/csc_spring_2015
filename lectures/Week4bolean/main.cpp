/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 16, 2015, 3:39 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    //User input uses while or do-while loop
    // single variable for loop 
    string userInput;
    
    cout << "would you like to play?";
    cin >> userInput;
    
    int chosenNumber = rand() %100;
    
    while (userInput == "yes")
    {
        cout << "Enter a number: ";
        int num;
        cin >> num;
        
        if (num == rand() % 100)
        {
            cout << "You win!";
        }
        else 
        {
            cout << "wrong" << endl;
        }
        // prompt the user to play again
        cout << "would you like to play again?" << endl;
        cin >> userInput;
    }
    return 0;
}

