/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on April 27, 2015, 3:15 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

// prototype
void computeCoin (int,int&,int&);
/*
 * 
 */
int main(int argc, char** argv) 
{
    
    cout << "Enter a change amount: ";
    int num;
    cin >>num;
    
    int numQuarters;
    int numDimes;
    int numPennies;
    int numNickles;
    
    computeCoin(25,numQuarters, num);
    computeCoin(10, numDimes, num);
    computeCoin(5, numNickles, num);
    computeCoin(1, numPennies, num);
    
    cout << "Quarters: " << numQuarters << endl;
    cout << "Dimes: " << numDimes << endl;
    cout << "Nickles: " << numNickles << endl;
    cout << "Pennies: " << numPennies << endl;
    return 0;
    
}

/**
 * computeCoin calculates the amount of coins in a given amount and given 
 * denomination.
 * subtracts the amount from amountLeft
 * 
 * returns nothing, void
 * @param denom is the given denomination
 * @param amount amount to be the coins in amountLeft
 * @param amountLeft amountLeft
 */
void computeCoin (int denom, int& amount,int& amountLeft)
{

    amount = amountLeft / denom;
    amountLeft %= denom;
}