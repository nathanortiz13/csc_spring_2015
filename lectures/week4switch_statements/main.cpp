/* 
 * File:   main.cpp
 * Author: rcc
 *
 * Created on March 9, 2015, 3:40 PM
 */

#include <cstdlib>
#include <iostream>
using namespace std;

/*
 * 
 */
int main(int argc, char** argv) {

    // use switch cases to create menu
    cout << "Please enter a number: " << endl;
    cout << "1 for Apples" << endl;
    cout << "2 for Oranges" << endl;
    cout << "3 for Grapes" << endl;
    
    //declare a variable for user input
    int num;
    cin >> num;
    
    switch(num)
    {
        case 1:
            cout << "Apples were selected.";
            break;
        case 2:
            cout << "Oranges were selected.";
            break;
        case 3:
            cout << "Grapes were selected";
            break;
        default:
            cout << "No fruit selected";
    }
    
    // String member functions
    //get a string from the user
    cout << endl << "Please enter a string: ";
    
    string userInput;
    cin >> userInput;
    
    cout << "The size of the string is: " << userInput.size() << endl;
    return 0;
}

